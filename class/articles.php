<?php
class Articles {
    private $_author;
    private $_title;
    private $_date_created;
    protected $_date_modified;

    public function __construct(string $title, string $author, string $date_created) {
        $this -> _title = $title;
        $this -> _author = $author;
        $this -> _date_created = $date_created;

        $this->hydrate ([
            "title" => $title,
            "author" => $author,
            "date_created" => $date_created
        ]);
    }

    private function hydrate(array $array) {
        foreach($array as $key => $value) {
        $method = "set" . ucfirst($key);
            if (method_exists($this, $method)) {
            $this->$method($value);
            }
        }
    }

    // Les Getters:
    public function getTitle () {
        return $this->_title;
    }

    public function getAuthor() {
        return $this-> _author;
    }

    public function getDate_created() {
        return $this->_date_created;
    }

    // Les Setters:
    
    protected function setTitle($title) {
        return $this-> _title = $title;
    }

    protected function setAuthor($author) {
        return $this-> _author = $author;
    }

    protected function setDate_created($date) {
        return $this-> _date_created = $date;
    }

    protected function setDate_modified($date) {
        return $this-> _date_modified = $date;
    }
}

?>