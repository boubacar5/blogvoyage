<?php
$title= "Accueil";
require "elements/header.php";
require "init.php";

?>

<div class="main" align="center">
    <div class="carousel">
        <img class="img" src="img/plage.jpg">
        <img class="img" src="img/alpes.jpg">
        <img class="img" src="img/sahara1.jpg">
        <img class="img" src="img/pyramide.jpg">
    </div>
        
    <div class="p1" >
        <h1>Nos voyages découverte</h1>
        <p>Nos itinéraires Découverte vous proposent d’appréhender un pays ou une région au rythme d’une marche tranquille sans difficulté. <br>
        Accessibles au plus grand nombre,nos voyages découverte associent sites incontournables de manière revisitée, <br>
        et itinéraires hors des sentiers battus, pour donner à voir <br>
        une destination sous ses aspects naturels, culturels et/ ou humain.</p>
    </div>

    <div class="article">
        <div>
            <img class="articleimg articleimg1" src="img/alpes.jpg" alt="">
            <h1>Montagne</h1>
            <p class="articleparagraphe">Assis dans le traîneau, vous vous laissez transporter
            <br> par le musher guidant l'attelageet vous profitez du paysage</p>
        </div>      
        <div>
            <img class="articleimg" src="img/pyramide.jpg" alt="">
            <h1>Pyramide Aztèque</h1>
            <p class="articleparagraphe">Suivez les traces des archéologues pour découvrir <br>
            les pyramides au Mexique</p>
        </div>
        <div>
            <img class="articleimg" src="img/plage.jpg" alt="">
            <h1>Plage</h1>
            <p class="articleparagraphe">Profitez d’un endroit plat et bas d'un rivage où les vagues déferlent.</p>
        </div>
        <div>
            <img class="articleimg" src="img/sahara1.jpg" alt=""  >
            <h1>Sahara</h1>
            <p class="articleparagraphe">Regardez le charmant coucher de soleil 
            <br>tout en montant le chameau avec la tenue du Sahara.
            </p>
        </div>
    </div>
</div>
<?php
require "elements/footer.php";
?>
