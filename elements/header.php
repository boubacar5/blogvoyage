<?php
if(isset($_POST["pseudo"]) && isset($_POST["mdp"])) {

}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/mobile.css" type="text/css">
    <script src="JS/script.js" defer></script>
    <title><?= $title ?></title>
</head>
<body>
    <header class ="header bgSecondaire" id="header">
        <nav class="nav" id="nav">
            <ul class="ul">
                <li><a href="#">Accueil</a></li>
                <li><a href="destination.php">Nos voyages</a></li>
                <li><a href="conseils.php">Conseils aux voyageurs</a></li>
            </ul>
        </nav>
        <div class="login" id="login">
            <form class="connection" action="#" method="POST">
                <label for="pseudo">Pseudonyme : </label>
                <input type="text" name="pseudo" required>
                <label for="mdp">Mot de passe : </label>
                <input type="text" name="mdp" required>
                <button type="submit" name="connecter">Connecter</button>
            </form>
            <a href="register.php" class="btn" id="btn">S'inscrire</a>
        </div>
    </header>
